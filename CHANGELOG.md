# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.5.3 - 2024-09-30]

- Fixed parsing for edge cases file without publisher

## [1.5.2 - 2024-09-06]

- Fixed parsing of flat displacement from bliss-tomo 2.6

## [1.5.1 - 2024-04-25]

### Fixed

- Fixed parsing of hdf5 from BLISS 2.0

## [1.5.0 - 2024-03-08]

### Added

- Added support to metadata from hdf5 file from 2022

### Changed

- Changed convertion of non completed scans can produre metadata files.
  A log error is displayed instead.

## [1.4.0 - 2024-03-04]

### Added

- Added support to `acc_operation` to normalize the output

### Changed

- Changed the way `acc_max_expo_time` is handled to normalize the output

### Removed

- Removed the check of the data depth to normalize the output

## [1.3.0 - UNRELEASED]

### Added

- Added `--skip-active-scans` to skip the scans which are not yet terminated
- Added `--raw-data-root` to change the behavior of retrieving the hdf5 files

### Changed

- Changed data access to the axis based on tomo config metadata

## [1.2.0 - 2023-10-16]

### Added

- Added `--current` to specify an external text file for the current

## [1.1.0 - 2023-10-11]

### Added

- Added `--debug` command line argument
- Added `--version` command line argument
- Added `--yml` command line argument to export a Yaml file with metadata
- Added `--dry-run` command line argument to go through the data without saving anything
- Added `--no-xml` command line argument to skip the xml export
- Added dataset for continuous integration test

### Changed

- Changed projection processing as stream
- Changed ulimit to the max available value
- Changed `h52edf` to `h5-to-edf` command line application
- Changed the progress bar with the one form fabio

### Fixed

- Fixed xml export
- Fixed RAM bottleneck, projections are read and write one by one without caching

## [1.0.3 - 2023-09-05]

### Added

- Added `h52edf` command line application
