import pytest
import pathlib


def pytest_addoption(parser):
    parser.addoption("--testdata", action="store", default=None)


class TestData:
    def __init__(self, root: str):
        self._root = pathlib.Path(root).absolute()

    @property
    def path(self):
        return self._root


@pytest.fixture(scope="session")
def testdata(request):
    testdata_path = request.config.getoption("--testdata")
    if testdata_path is None:
        pytest.skip("--testdata was not specified")
    testdata = TestData(testdata_path)
    if not testdata.path.exists():
        raise ValueError(f"testdata path {testdata_path} does not exist")
    yield testdata
