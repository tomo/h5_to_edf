import os
import numpy
import pytest
from h5_to_edf.edf_file_series import EdfFileSeries


def test_empty(tmp_path):
    output = tmp_path
    with EdfFileSeries(output, "frame_{index:04d}.edf") as s:
        pass


def test_single(tmp_path):
    output = tmp_path
    d = numpy.arange(16, dtype=numpy.uint32).reshape(4, 4)
    with EdfFileSeries(output, "frame_{index:04d}.edf") as s:
        s.append(d, {"foo": "0"})
    assert os.path.exists(output / "frame_0000.edf")


def test_multi(tmp_path):
    output = tmp_path
    d = numpy.arange(16, dtype=numpy.uint32).reshape(4, 4)
    with EdfFileSeries(output, "frame_{index:04d}.edf") as s:
        s.append(d + 0, {"foo": "0"})
        s.append(d + 1, {"foo": "1"})
        s.append(d + 2, {"foo": "2"})
    assert os.path.exists(output / "frame_0002.edf")


def test_dry(tmp_path):
    output = tmp_path
    with EdfFileSeries(output, "frame_{index:04d}.edf", dry_run=False) as s:
        pass
    assert not os.path.exists(output / "frame_0000.edf")


def test_first_index(tmp_path):
    output = tmp_path
    d = numpy.arange(16, dtype=numpy.uint32).reshape(4, 4)
    with EdfFileSeries(output, "frame_{index:04d}.edf", first_index=10) as s:
        s.append(d + 0, {"foo": "0"})
        s.append(d + 1, {"foo": "1"})
        s.append(d + 2, {"foo": "2"})
    assert not os.path.exists(output / "frame_0000.edf")
    assert os.path.exists(output / "frame_0010.edf")
    assert os.path.exists(output / "frame_0011.edf")
    assert os.path.exists(output / "frame_0012.edf")
    assert not os.path.exists(output / "frame_0013.edf")


def test_reuse_context_not_allowed(tmp_path):
    output = tmp_path
    s = EdfFileSeries(output, "frame_{index:04d}.edf")
    d = numpy.arange(16, dtype=numpy.uint32).reshape(4, 4)
    with s:
        s.append(d, {"foo": "0"})
    with pytest.raises(RuntimeError):
        with s:
            pass
