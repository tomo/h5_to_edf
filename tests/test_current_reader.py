import os
import datetime
import numpy
import pytest
import datetime

from h5_to_edf.current_reader import CurrentReader


def test_before():
    current = CurrentReader()
    current.set_values([1, 2, 3, 4], [0.1, 0.2, 0.3, 0.4])
    assert current.get(-1) == 0.1


def test_first():
    current = CurrentReader()
    current.set_values([1, 2, 3, 4], [0.1, 0.2, 0.3, 0.4])
    assert current.get(1) == 0.1


def test_mid():
    current = CurrentReader()
    current.set_values([1, 2, 3, 4], [0.1, 0.2, 0.3, 0.4])
    assert current.get(2) == 0.2


def test_last():
    current = CurrentReader()
    current.set_values([1, 2, 3, 4], [0.1, 0.2, 0.3, 0.4])
    assert current.get(4) == 0.4


def test_after():
    current = CurrentReader()
    current.set_values([1, 2, 3, 4], [0.1, 0.2, 0.3, 0.4])
    assert current.get(5) == 0.4


ESRF_CURRENT_TEXT_FILE = """\
# File generated from hdbviewer application
#
HDB Date	HDB Time	srdiag/beam-current/total/current (mA)	
31/08/2023	08:00:00	64.20190708117754
31/08/2023	08:00:01	64.19884872759694
31/08/2023	08:00:02	64.19626368849961
31/08/2023	08:00:03	64.19361641459122
31/08/2023	08:00:04	64.19091773322933
31/08/2023	08:00:05	64.18816038618533
"""


def test_text_current(tmp_path):
    filename = tmp_path / "current_20230831"
    with open(filename, "wt") as f:
        f.write(ESRF_CURRENT_TEXT_FILE)
    current = CurrentReader()
    current.read_esrf_current_file(filename)

    def to_utc_timestamp(string: str):
        d = datetime.datetime.fromisoformat(string)
        d = d.replace(tzinfo=datetime.timezone.utc)
        return d.timestamp()

    assert current.get(to_utc_timestamp("2023-08-31 08:00:02")) == 64.19626368849961
    assert (
        current.get(to_utc_timestamp("2023-08-31 08:00:02") + 0.3) == 64.19626368849961
    )
    assert current.get(to_utc_timestamp("2023-08-31 08:00:05")) == 64.18816038618533
