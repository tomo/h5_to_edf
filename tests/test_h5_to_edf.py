import os
import fabio


def test_bm05_zf13_align(tmp_path, testdata):
    path = testdata.path / "bm05" / "zf13_align_test_hr_coord0.h5"
    cmd = f"h52edf {path} -o {tmp_path} --yml --debug"
    print(cmd)
    r = os.system(cmd)
    assert r == 0

    result_path = tmp_path / "bm05_"
    assert (result_path / "angle_proj.dat").exists()
    assert (result_path / "bm05_.info").exists()
    assert (result_path / "zf13_align_.xml").exists()
    assert (result_path / "zf13_align_.yml").exists()

    assert (result_path / "bm05_0000.edf").exists()
    assert (result_path / "bm05_1000.edf").exists()
    assert (result_path / "bm05_1001.edf").exists()
    assert (result_path / "bm05_1005.edf").exists()
    assert (result_path / "dark.edf").exists()
    assert (result_path / "refHST0000.edf").exists()
    assert (result_path / "refHST1000.edf").exists()
    assert not (result_path / "bm05_1006.edf").exists()

    with fabio.open(result_path / "bm05_0500.edf") as f:
        assert float(f.header["SRCUR"]) == 198.17
        assert f.header["motor_mne"] == "sx sy sz yrot"
        assert f.header["motor_pos"] == "-1.11 0.416 -92.828 1.6799999999999997"
    with fabio.open(result_path / "bm05_1003.edf") as f:
        assert "SRCUR" not in f.header
        assert "motor_mne" not in f.header


def test_bm05_zf13_align__external_current(tmp_path, testdata):
    path = testdata.path / "bm05_nocurrent" / "zf13_align_test_hr_coord0.h5"
    current = testdata.path / "bm05_nocurrent" / "current_20230913"
    cmd = f"h52edf {path} -o {tmp_path} --yml --debug --current={current}"
    print(cmd)
    r = os.system(cmd)
    assert r == 0

    result_path = tmp_path / "bm05_nocurrent_"
    assert (result_path / "angle_proj.dat").exists()
    assert (result_path / "bm05_nocurrent_.info").exists()
    assert (result_path / "zf13_align_.xml").exists()
    assert (result_path / "zf13_align_.yml").exists()

    assert (result_path / "bm05_nocurrent_0000.edf").exists()
    assert (result_path / "bm05_nocurrent_1000.edf").exists()
    assert (result_path / "bm05_nocurrent_1001.edf").exists()
    assert (result_path / "bm05_nocurrent_1005.edf").exists()
    assert (result_path / "dark.edf").exists()
    assert (result_path / "refHST0000.edf").exists()
    assert (result_path / "refHST1000.edf").exists()
    assert not (result_path / "bm05_nocurrent_1006.edf").exists()

    with fabio.open(result_path / "bm05_nocurrent_0500.edf") as f:
        assert float(f.header["SRCUR"]) == 198.17
        assert f.header["motor_mne"] == "sx sy sz yrot"
        assert f.header["motor_pos"] == "-1.11 0.416 -92.828 1.6799999999999997"
    with fabio.open(result_path / "bm05_nocurrent_1003.edf") as f:
        assert "SRCUR" not in f.header
        assert "motor_mne" not in f.header


def test_bm18_oxtail_air_zseries(tmp_path, testdata):
    path = (
        testdata.path
        / "bm18_oxtail_air_zseries/2.015um_oxtail_air/HA900_2.015um_oxtail_air__0001/md1389_HA900_2.015um_oxtail_air__0001.h5"
    )
    cmd = f"h52edf {path} -o {tmp_path} --yml --debug"
    print(cmd)
    r = os.system(cmd)
    assert r == 0

    result_path = tmp_path / "HA900_2.015um_oxtail_air__0001_"
    assert (result_path / "angle_proj.dat").exists()
    assert (result_path / "HA900_2.015um_oxtail_air__0001_.info").exists()
    assert (result_path / "HA900_2.015um_oxtail_air__0001_.xml").exists()
    assert (result_path / "HA900_2.015um_oxtail_air__0001_.yml").exists()

    assert (result_path / "HA900_2.015um_oxtail_air__0001_0000.edf").exists()
    assert (result_path / "HA900_2.015um_oxtail_air__0001_7999.edf").exists()
    assert (result_path / "HA900_2.015um_oxtail_air__0001_8000.edf").exists()
    assert (result_path / "dark.edf").exists()
    assert (result_path / "refHST0000.edf").exists()
    assert (result_path / "refHST8000.edf").exists()
    assert not (result_path / "HA900_2.015um_oxtail_air__8001.edf").exists()

    with fabio.open(result_path / "HA900_2.015um_oxtail_air__0001_0500.edf") as f:
        assert float(f.header["SRCUR"]) == 193.7
        assert f.header["motor_mne"] == "sx sy sz yrot"
        assert f.header["motor_pos"] == "-5.534 7.0 -406.5 1.8134000000000001"


def test_step_scan(tmp_path, testdata):
    path = (
        testdata.path / "daiquiri_demo" / "sample2" / "sample2_test" / "sample2_test.h5"
    )
    cmd = f"h52edf {path} -o {tmp_path} --yml --debug"
    print(cmd)
    r = os.system(cmd)
    assert r == 0

    result_path = tmp_path / "sample2_test_"
    assert (result_path / "sample2_test_0000.edf").exists()
    assert (result_path / "dark.edf").exists()
    assert (result_path / "sample2_test_.info").exists()
    assert (result_path / "sample2_.xml").exists()
    assert (result_path / "sample2_.yml").exists()

    result_path = tmp_path / "bm05_"
    assert not result_path.exists()


def test_failed_on_active_scans(tmp_path, testdata):
    path = testdata.path / "bm05_unclosed" / "unclosed.h5"
    cmd = f"h52edf {path} -o {tmp_path} --yml --debug"
    print(cmd)
    r = os.system(cmd)
    assert r != 0

    result_path = tmp_path / "bm05_"
    assert not result_path.exists()


def test_skipped_active_scans(tmp_path, testdata):
    path = testdata.path / "bm05_unclosed" / "unclosed.h5"
    cmd = f"h52edf {path} -o {tmp_path} --yml --debug --skip-active-scans"
    print(cmd)
    r = os.system(cmd)
    assert r == 0

    result_path = tmp_path / "bm05_"
    assert not result_path.exists()


def check_output(result_path, dataset_name: str, sample_name: str):
    assert (result_path / f"{dataset_name}_0000.edf").exists()
    assert (result_path / "dark.edf").exists()
    assert (result_path / "refHST0000.edf").exists()
    assert (result_path / f"{dataset_name}_.info").exists()
    assert (result_path / f"{sample_name}_.xml").exists()
    assert (result_path / f"{sample_name}_.yml").exists()


def test_collection_datasets(tmp_path, testdata):
    path = testdata.path / "daiquiri_demo"
    cmd = f"h52edf {path} -o {tmp_path} --yml --debug --raw-data-root"
    print(cmd)
    r = os.system(cmd)
    assert r == 0

    check_output(tmp_path / "sample2" / "sample2_test_", "sample2_test", "sample2")
    check_output(
        tmp_path / "sample3" / "sample3_test_000_", "sample3_test_000", "sample3"
    )
    check_output(
        tmp_path / "sample3" / "sample3_test_001_", "sample3_test_001", "sample3"
    )
    check_output(
        tmp_path / "sample3" / "sample3_test_002_", "sample3_test_002", "sample3"
    )


def test_bm05_2022(tmp_path, testdata):
    path = testdata.path / "bm05_2022" / "dataset.h5"
    cmd = f"h52edf {path} -o {tmp_path} --yml --debug"
    print(cmd)
    r = os.system(cmd)
    assert r == 0

    result_path = tmp_path / "bm05_2022_"
    assert (result_path / "bm05_2022_0000.edf").exists()
    assert (result_path / "bm05_2022_0088.edf").exists()
    assert not (result_path / "bm05_2022_0089.edf").exists()
    assert (result_path / "HA900_2.42um_BP_1_4242_L_femur_.yml").exists()
    assert (result_path / "angle_proj.dat").exists()


def test_simu_bliss_2_0(tmp_path, testdata):
    path = testdata.path / "simu" / "bliss2.0" / "simu.h5"
    current = testdata.path / "simu" / "bliss2.0" / "current"
    cmd = f"h52edf {path} -o {tmp_path} --no-xml --current={current} --debug"
    print(cmd)
    r = os.system(cmd)
    assert r == 0

    result_path = tmp_path / "bliss2.0_"
    assert (result_path / "bliss2.0_0000.edf").exists()
    assert (result_path / "bliss2.0_0091.edf").exists()
    assert not (result_path / "bliss2.0_0092.edf").exists()
    assert (result_path / "bliss2.0_.info").exists()
    assert (result_path / "angle_proj.dat").exists()


def test_simu_bliss_2_0_tomo_2_6(tmp_path, testdata):
    path = testdata.path / "simu" / "bliss2.0_tomo2.6" / "fulltomo" / "id002409_id00.h5"
    current = testdata.path / "simu" / "bliss2.0" / "current"
    cmd = f"h52edf {path} -o {tmp_path} --current={current} --debug"
    print(cmd)
    r = os.system(cmd)
    assert r == 0

    result_path = tmp_path / "fulltomo_"
    assert (result_path / "fulltomo_0000.edf").exists()
    assert (result_path / "fulltomo_0006.edf").exists()
    assert not (result_path / "fulltomo_0007.edf").exists()
    assert (result_path / "fulltomo_.info").exists()
    assert (result_path / "sample_.xml").exists()
    assert (result_path / "angle_proj.dat").exists()


def test_h5py_3_6(tmp_path, testdata):
    path = testdata.path / "edge_cases" / "h5py_3.6" / "id002409_id00.h5"
    current = testdata.path / "simu" / "bliss2.0" / "current"
    cmd = f"h52edf {path} -o {tmp_path} --current={current} --debug"
    print(cmd)
    r = os.system(cmd)
    assert r == 0

    result_path = tmp_path / "h5py_3.6_"
    assert (result_path / "h5py_3.6_0000.edf").exists()
